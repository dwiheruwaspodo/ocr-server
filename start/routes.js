'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.on('/').render('welcome')
Route.get('hello', 'UserController.hello')

/* GROUP AUTH */
const group_no_auth = (group) => {
  group.prefix('api')
  return group
}

group_no_auth(Route.group(() => {
  Route.get('login', 'AuthController.postLoginJwt').prefix('jwt')
  Route.post('register', 'UserController.register').prefix('user').validator('Register')

  /* OCR */
	Route.post('go-ocr', 'OcrController.ocr')

  /* UPLOAD */
  Route.post('upload', 'OcrController.upload')
}))

/* GROUP JWT */
const group_auth = (group) => {
  group.middleware(['auth:jwt']).prefix('api')
  return group
}

group_auth(Route.group(() => {
	Route.get('profile', 'AuthController.getProfileJwt').prefix('jwt')
    Route.post('logout', 'AuthController.postLogoutJwt').prefix('jwt')
    Route.post('refresh', 'AuthController.postRefreshTokenJwt').prefix('jwt')
    	
    Route.get('user', 'UserController.userVehicle').prefix('data')
    Route.get('vehicle', 'UserController.vehiclesUser').prefix('data')

}))

