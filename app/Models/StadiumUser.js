'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class StadiumUser extends Model {
	static get table () {
		return 'stadium_users'
	}

	static get primaryKey() {
		return 'ids_stadium_user'
	}

	priceList () {
	    return this.hasMany('App/Models/StadiumPrice', 'ids_stadium_user', 'ids_stadium_user')
	}

	stadium() {
		return this.belongsTo('App/Models/Stadium', 'ids_stadium', 'ids_stadium')
	}
}

module.exports = StadiumUser
