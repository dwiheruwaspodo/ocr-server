'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class VehicleCategory extends Model {
	static get table () {
		return 'vehicle_categories'
	}

	static get primaryKey() {
		return 'idv_category'
	}

	brands () {
	    return this.hasMany('App/Models/VehicleBrand', 'idv_category', 'idv_category')
	}
}

module.exports = VehicleCategory
