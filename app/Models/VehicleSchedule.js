'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class VehicleSchedule extends Model {
	static get table () {
		return 'vehicle_schedules'
	}

	static get primaryKey () {
	    return 'idv_schedule'
	}

	vehicleUser() {
		return this.belongsTo('App/Models/VehicleUser', 'idv_vehicle_user', 'idv_vehicle_user')
	}
}

module.exports = VehicleSchedule
