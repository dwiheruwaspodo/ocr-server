'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Stadium extends Model {
	static get table () {
		return 'stadiums'
	}

	static get primaryKey() {
		return 'ids_stadium'
	}
}

module.exports = Stadium
