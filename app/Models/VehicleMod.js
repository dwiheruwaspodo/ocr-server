'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class VehicleMod extends Model {
	static get table () {
		return 'vehicle_models'
	}

	static get primaryKey () {
	    return 'idv_model'
	}

	vehicleUsers () {
	    return this.hasMany('App/Models/VehicleUser', 'idv_model', 'idv_model')
	}

	type() {
		return this.belongsTo('App/Models/VehicleType', 'idv_type', 'idv_type')
	}
}

module.exports = VehicleMod
