'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class StadiumScheduleDetail extends Model {
	static get table () {
		return 'stadium_schedule_details'
	}

	static get primaryKey() {
		return 'ids_schedule_detail'
	}
}

module.exports = StadiumScheduleDetail
