'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class VehicleUser extends Model {
	static get table () {
		return 'vehicles'
	}

	static get primaryKey () {
	    return 'idv_vehicle_user'
	}

	terms () {
	    return this.hasMany('App/Models/VehicleUserTerm', 'idv_vehicle_user', 'idv_vehicle_user')
	}

	model() {
		return this.belongsTo('App/Models/VehicleMod', 'idv_model', 'idv_model')
	}
}

module.exports = VehicleUser
