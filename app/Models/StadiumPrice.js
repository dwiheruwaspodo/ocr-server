'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class StadiumPrice extends Model {
	static get table () {
		return 'stadium_prices'
	}

	static get primaryKey() {
		return 'ids_price'
	}
}

module.exports = StadiumPrice
