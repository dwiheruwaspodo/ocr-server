'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class VehicleBrand extends Model {
	static get table () {
		return 'vehicle_brands'
	}

	static get primaryKey() {
		return 'idv_brand'
	}

	type () {
	    return this.hasMany('App/Models/VehicleType', 'idv_brand', 'idv_brand')
	}

	categories() {
		return this.belongsTo('App/Models/VehicleCategory', 'idv_category', 'idv_category')
	}
}

module.exports = VehicleBrand
