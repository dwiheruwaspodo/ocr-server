'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class VehicleUserTerm extends Model {
	static get table () {
		return 'vehicle_user_terms'
	}
	
	static get primaryKey () {
	    return 'idv_term'
	}
}

module.exports = VehicleUserTerm
