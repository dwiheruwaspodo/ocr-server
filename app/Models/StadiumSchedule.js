'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class StadiumSchedule extends Model {
	static get table () {
		return 'stadium_schedules'
	}

	static get primaryKey() {
		return 'ids_schedule'
	}

	detailSchedule () {
	    return this.hasMany('App/Models/StadiumScheduleDetail', 'ids_schedule', 'ids_schedule')
	}
}

module.exports = StadiumSchedule
