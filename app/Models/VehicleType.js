'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class VehicleType extends Model {
	static get table () {
		return 'vehicles'
	}

	static get primaryKey () {
	    return 'idv_type'
	}

	models () {
	    return this.hasMany('App/Models/VehicleMod', 'idv_type', 'idv_type')
	}

	brands() {
		return this.belongsTo('App/Models/VehicleBrand', 'idv_brand', 'idv_brand')
	}
}

module.exports = VehicleType
