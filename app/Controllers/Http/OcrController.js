'use strict'

const Tesseract = require('tesseract.js')
const Helpers = use('Helpers')
const base64Img = require('base64-img');
class OcrController {

	async ocr({request, response}) {
		// console.log(request)
		const resourcesPath = Helpers.publicPath('ktp.jpg')

		await Tesseract.recognize(resourcesPath, {
		    lang: 'eng'
		})
       	.progress(function  (p) { console.log('progress', p)    })
       	.catch(err => console.error(err))
    	.then(function(result){
    		// console.log(typeof result)
    		return response.json({'text' : result.text})
    		// console.log('result is: ', result)
    	})
    	.finally(resultOrError => console.log(resultOrError))
		
		// return response.send(resourcesPath)
	}

	async upload({request, response}) {
		var pathImg = base64Img.imgSync(request.input('image'), Helpers.publicPath('photos'), 'testing');
		
		return response.json({
			'path': pathImg,
			'err' : "err"
		})
		// console.log(image)

		
	}
}

module.exports = OcrController
