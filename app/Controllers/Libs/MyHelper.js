'use strict'

const returnApi = (data) => {
	// console.log(data)
	// console.log(data.rows)
  	var result = {}

  	// if (!(!Object.keys(data).length)) {
  	if (data.rows.length > 0) {
  		Object.assign(result, {status: "success", data: data})
  	}
  	else {
  		Object.assign(result, {status: "fail"})
  	}

  	return result
}

module.exports = { returnApi }