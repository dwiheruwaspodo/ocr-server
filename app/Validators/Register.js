'use strict'

const { formatters } = use('Validator')

class Register {
  	get rules () {
  		// console.log(this.ctx.request.all())
    	return {
      		// validation rules
			username : 'required',
			email    : 'required|email|unique:users,email',
			password : 'required'
    	}
  	}

  	get messages () {
  	   	return {
			'username.required' : 'username is required.',
			'password.required' : 'password is required.',
			'email.required'    : 'email is required',
			'email.unique'      : 'email must be unique',
			'email.email'       : 'email format is not invalid'
  		}
  	}

  	async fails (errorMessages) {
  	    return this.ctx.response.send(errorMessages)
  	}

  	get data () {
	    const requestBody = this.ctx.request.all()
	    const sessionId = this.ctx.request.header('X-Session-Id')

	    return Object.assign({}, requestBody, { sessionId })
	}

	get formatter () {
	    return formatters.JsonApi
	}
}

module.exports = Register
