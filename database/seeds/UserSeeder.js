'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const User = use('App/Models/User')

class UserSeeder {
  async run () {
    const u1    = new User()
    u1.username = 'waspodov'
    u1.password = 'waspodov'
    u1.email    = 'waspodov@mail.com'
    u1.phone    = "083847090002"
    await u1.save()

    const u2    = new User()
    u2.username = 'citcit'
    u2.password = 'citcit'
    u2.email    = 'citcit@mail.com'
    u2.phone    = "083847090001"
    await u2.save()
  }
}

module.exports = UserSeeder

