'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VehicleCategorySchema extends Schema {
  up () {
    this.create('vehicle_categories', (table) => {
    	table.increments('idv_category')
    	table.string('category', 100)
      	table.timestamps()
    })
  }

  down () {
    this.drop('vehicle_categories')
  }
}

module.exports = VehicleCategorySchema
