'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StadiumScheduleSchema extends Schema {
  up () {
    this.create('stadium_schedules', (table) => {
      table.increments('ids_schedule')
      table.string('code_booking', 20).nullable()
      table.integer('ids_stadium_user').unsigned().references('ids_stadium_user').inTable('stadium_users').onDelete('set null')
      table.integer('user_order').unsigned().references('id').inTable('users').onDelete('set null')
      table.enu('status', ['bid', 'connect', 'cancel']).nullable()
      table.integer('subtotal')
      table.integer('discount')
      table.integer('grandtotal')
      table.timestamps()
    })
  }

  down () {
    this.drop('stadium_schedules')
  }
}

module.exports = StadiumScheduleSchema
