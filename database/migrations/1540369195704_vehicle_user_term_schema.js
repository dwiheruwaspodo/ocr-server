'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VehicleUserTermSchema extends Schema {
  up () {
    this.create('vehicle_user_terms', (table) => {
        table.increments('idv_term')
        table.integer('idv_vehicle_user').unsigned().references('idv_vehicle_user').inTable('vehicle_users').onDelete('set null')
        table.string('term', 200).nullable()
        table.timestamps()
    })
  }

  down () {
    this.drop('vehicle_user_terms')
  }
}

module.exports = VehicleUserTermSchema
