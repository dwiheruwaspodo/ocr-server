'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VehicleModelSchema extends Schema {
  up () {
    this.create('vehicle_models', (table) => {
      	table.increments('idv_model')
      	table.integer('idv_type').unsigned().references('idv_type').inTable('vehicle_types').onDelete('set null')
      	table.string('model', 100)
      	table.string('year', 4)
      	table.timestamps()
    })
  }

  down () {
    this.drop('vehicle_models')
  }
}

module.exports = VehicleModelSchema
