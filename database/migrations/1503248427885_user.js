'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('username', 80).notNullable()
      table.string('email', 254).notNullable().unique()
      table.string('phone', 20).notNullable().unique()
      table.string('password', 60).notNullable()
      table.string('ktp', 20).nullable()
      table.string('ktp_scan', 100).nullable()
      table.string('address', 200).nullable()
      table.integer('id_city').nullable()
      table.string('latitude', 100).nullable()
      table.string('longitude', 100).nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
