'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StadiumScheduleDetailSchema extends Schema {
  up () {
    this.create('stadium_schedule_details', (table) => {
      table.increments('ids_schedule_detail')
      table.integer('ids_schedule').unsigned().references('ids_schedule').inTable('stadium_schedules').onDelete('set null')
      table.integer('ids_price').unsigned().references('ids_price').inTable('stadium_prices').onDelete('set null')
      table.time('time_start')
      table.time('time_end')
      table.integer('price')
      table.integer('subtotal')
      table.integer('discount')
      table.timestamps()
    })
  }

  down () {
    this.drop('stadium_schedule_details')
  }
}

module.exports = StadiumScheduleDetailSchema
