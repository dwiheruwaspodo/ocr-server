'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UpdateVehicleSchedulesSchema extends Schema {
  up () {
    this.table('vehicle_schedules', (table) => {
      table.integer('user_order').unsigned().references('id').inTable('users').onDelete('set null').after('rent_start')
      table.string('code_booking', 20).nullable().after('user_order')
    })
  }

  down () {
    this.table('vehicle_schedules', (table) => {
      // table.dropColumn('user_order')
      // table.dropColumn('code_booking')
    })
  }
}

module.exports = UpdateVehicleSchedulesSchema
