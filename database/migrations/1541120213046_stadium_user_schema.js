'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StadiumUserSchema extends Schema {
  up () {
    this.create('stadium_users', (table) => {
      table.increments('ids_stadium_user')
      table.integer('id_user').unsigned().references('id').inTable('users').onDelete('set null')
      table.integer('ids_stadium').unsigned().references('ids_stadium').inTable('stadiums').onDelete('set null')
      table.timestamps()
    })
  }

  down () {
    this.drop('stadium_users')
  }
}

module.exports = StadiumUserSchema
