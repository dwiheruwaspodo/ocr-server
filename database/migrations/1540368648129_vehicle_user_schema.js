'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VehicleUserSchema extends Schema {
  up () {
    this.create('vehicle_users', (table) => {
      	table.increments('idv_vehicle_user')
      	table.integer('idv_model').unsigned().references('idv_model').inTable('vehicle_models').onDelete('set null')
      	table.integer('id_user').unsigned().references('id').inTable('users').onDelete('set null')
      	table.integer('price_12')
      	table.integer('price_24')
      	table.string('police_number').nullable()
      	table.enu('status', ['available', 'used']).defaultTo('available')
      	table.timestamps()
    })
  }

  down () {
    this.drop('vehicle_users')
  }
}

module.exports = VehicleUserSchema
