'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VehicleTypeSchema extends Schema {
  up () {
    this.create('vehicle_types', (table) => {
    	table.increments('idv_type')
    	table.integer('idv_brand').unsigned().references('idv_brand').inTable('vehicle_brands').onDelete('set null')
    	table.string('type', 100)
      	table.timestamps()
    })
  }

  down () {
    this.drop('vehicle_types')
  }
}

module.exports = VehicleTypeSchema
