'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VehicleScheduleSchema extends Schema {
  up () {
    this.create('vehicle_schedules', (table) => {
      table.increments('idv_schedule')
      table.integer('idv_vehicle_user').unsigned().references('idv_vehicle_user').inTable('vehicle_users').onDelete('set null')
      table.dateTime('rent_start')
      table.dateTime('rent_end')
      table.date('pickup_date')
      table.time('pickup_time')
      table.string('pickup_address', 255)
      table.string('pickup_lat', 100).nullable()
      table.string('pickup_long', 100).nullable()
      table.enu('status', ['bid', 'connect', 'cancel']).nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('vehicle_schedules')
  }
}

module.exports = VehicleScheduleSchema
