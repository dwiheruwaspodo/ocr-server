'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StadiumSchema extends Schema {
  up () {
    this.create('stadiums', (table) => {
      table.increments('ids_stadium')
      table.string('stadium', 100)
      table.string('stadium_code', 20).nullable()
      table.string('address', 255)
      table.string('phone', 20)
      table.string('email', 60)
      table.string('social01', 100).nullable()
      table.string('social02', 100).nullable()
      table.string('social03', 100).nullable()
      table.time('time_open').nullable()
      table.time('time_close').nullable()
      table.enu('status', ['available', 'holiday', 'unsub']).defaultTo('available')
      table.timestamps()
    })
  }

  down () {
    this.drop('stadiums')
  }
}

module.exports = StadiumSchema
