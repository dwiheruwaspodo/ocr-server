'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StadiumPriceSchema extends Schema {
  up () {
    this.create('stadium_prices', (table) => {
      table.increments('ids_price')
      table.integer('ids_stadium_user').unsigned().references('ids_stadium_user').inTable('stadium_users').onDelete('set null')
      table.time('time_start')
      table.time('time_end')
      table.integer('price')
      table.timestamps()
    })
  }

  down () {
    this.drop('stadium_prices')
  }
}

module.exports = StadiumPriceSchema
